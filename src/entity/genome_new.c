
#include "tiny-verse.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_genome		*genome_new(uint16_t length)
{
	t_genome	*new_genome;

	new_genome = malloc(sizeof(t_genome) + length);
	if (new_genome == NULL)
		return (NULL);
	new_genome->length = length;
	ct_bzero(new_genome->genes, length);
	return (new_genome);
}
