
#include "tiny-verse.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_genome		*genome_new(uint16_t length)
{
	uint16_t	total_length;
	t_genome	*new_genome;

	total_length = GENOME_HEADER_LENGTH + length;
	if (total_length < length)
		return (NULL);
	new_genome = malloc(sizeof(t_genome) + total_length);
	if (new_genome == NULL)
		return (NULL);
	new_genome->length = length;
	ct_memset(new_genome->genes, '0', total_length);
	return (new_genome);
}
